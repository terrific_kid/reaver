var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
var forks = [];
const { fork } = require('child_process');
function exitHandler(error) {
  if(error)reaver.error('execute', 'exit', error);
  process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });


async function start(executor){
        forks[executor] = fork('/reaver/executors/'+executor+'.js');
        forks[executor].on('exit', (error) => { console.error(error); start(executor);  });
}

async function monitor() {
       await reaver.del('queue');
       var trade = await reaver.blpop('queue',100);

       if(trade){
          var trade = JSON.parse(trade[1]);
         reaver.log('monitor', '', trade);
         
         /*
          var cost = trade.marketAskPrice * trade.qty;
          var net = trade.marketBidPrice * trade.qty;
          
          //Check for funds/pending
          //var check = await reaver.mget(trade.setA+trade.base, trade.setB+trade.token, 'trades');
          if(0){
          //if(check[0] < cost || check[1] < trade.qty || check[2] > 0){
            reaver.error('execute', 'trade fail', [trade.setA+check[0]+trade.base, trade.setA+cost+trade.base, trade.setB+check[1]+trade.token, trade.setB+trade.qty+trade.token]);
          }else{
            
            reaver.log('execute', 'trade');
          
            //forks[trade.setA].send({type: 'buy', qty: trade.qty, price: trade.marketAskPrice, token: trade.token, base: trade.base });
            //forks[trade.setB].send({type: 'sell', qty: trade.qty, price: trade.marketBidPrice, token: trade.token, base: trade.base });
          }
          */
        
       }
      
      monitor();
};

async function intervalFunc() {
    var msgs = await reaver.zrangebyscore('monitor', reaver.now(), '+inf');
    
      if(!msgs[0]) return;
      log = JSON.parse(msgs[0]);
      reaver.log(log[0],log[1],log[2]);
    
     
     }
 

(async function(){
  
   setInterval(intervalFunc,1000);
  //monitor();

})();

 
 

 
