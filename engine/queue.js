var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const { fork } = require('child_process');
 var forks = {};
 function exitHandler(debug) {
    reaver.error('queue', 'exit', debug);
    for(f of Object.values(forks)) f.kill("SIGTERM");
    process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });


//RUN
(async function() {
  reaver.log('queue', 'init');
    process.on('message', message => {
        alchemy();
    });
})();


async function queue(msg){
  reaver.lpush('queue', JSON.stringify(msg));
  reaver.log('alchemy', 'queue', []);
}


async function alchemy(){
         
         var list = [];
         for(loader of reaver.config.loaders){
          list.push(loader+'_'+reaver.config.token+reaver.config.base);
         }
         var data = await reaver.mget(list);
         
        var i = 0;
        var book = [];
        for(item of list){
          book.push([list[i],JSON.parse(data[i])]);
          i++;
        }
        await Promise.all(book.map(async (setA) => {
            await Promise.all(book.map(async (setB) => {
               
                var setAeid = setA[0].split('_')[0];
                var setBeid = setB[0].split('_')[0];
                if(setAeid == setBeid) return;
                if(!setA[1] || !setB[1]){ 
                  console.error('No Book!');
                  return; } 
                //reaver.log('alchemy', 'calc');
                var qty = reaver.round((reaver.config.min / setA[1].asks[0].price),reaver.config.pt);
                var alchemy = reaver.alchemize(qty, setA[1], setB[1]);
                
                if(alchemy.ny > 0){
                    queue({type: 'trade', ny: reaver.round(alchemy.ny,3), setA: setAeid, setB: setBeid, marketAskPrice: alchemy.marketAskPrice, askqty: alchemy.askqty, marketBidPrice: alchemy.marketBidPrice, bidqty: alchemy.bidqty, qty: qty, token: reaver.config.token, base: reaver.config.base});
                }else{
                   return 0;
                }
            }));
        }));
         
  }



 
  

