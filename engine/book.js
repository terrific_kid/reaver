var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const { fork } = require('child_process');
 var forks = {};
function exitHandler(debug) {
     reaver.error('book', 'exit', debug);
    for(f of Object.values(forks)) f.kill("SIGTERM");
    process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });
async function start(loader){
        forks[loader] = fork('/reaver/loaders/'+loader+'.js', [process.argv[2]]);
        forks[loader].on('message', (msg) => { process.send(msg); });
        forks[loader].on('exit', (error) => { exitHandler(error); });
  }
//RUN
(async function() {
   reaver.log('book', 'init');
  await Promise.all(reaver.config.loaders.map(async (loader) => { 
   start(loader);
  }));

})();



  