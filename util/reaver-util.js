module.exports =  function(){
    
    var fs = require('fs');
    const {promisify} = require('util');
    var https = require('https');
    this.math = require('mathjs')
    this.req = function(params, postData) {
    return new Promise(function(resolve, reject) {
            var req = https.request(params, function(res) {
                // reject on bad status
                if (res.statusCode < 200 || res.statusCode >= 300) {
                    return reject(new Error('statusCode=' + res.statusCode));
                }
                // cumulate data
                var body = [];
                res.on('data', function(chunk) {
                    body.push(chunk);
                });
                // resolve on end
                res.on('end', function() {
                    try {
                        body = JSON.parse(Buffer.concat(body).toString());
                    } catch(e) {
                        reject(e);
                    }
                    resolve(body);
                });
            });
            // reject on request error
            req.on('error', function(err) {
                // This is not a "Second reject", just a different sort of failure
                reject(err);
            });
            if (postData) {
                req.write(postData);
            }
            // IMPORTANT
            req.end();
        });
    }

    this.redis = require("redis");
    this.db = new this.redis.createClient();
    this.db.on("error", function (err) {
        console.log("Error " + err);
    });
    this.get = promisify(this.db.get).bind(this.db);
    this.mget = promisify(this.db.mget).bind(this.db);
    this.set = promisify(this.db.set).bind(this.db);
    this.append = promisify(this.db.append).bind(this.db);
    this.getset = promisify(this.db.getset).bind(this.db);
    this.lpush = promisify(this.db.lpush).bind(this.db);
    this.blpop = promisify(this.db.blpop).bind(this.db);
    this.del = promisify(this.db.del).bind(this.db);
    this.incrby = promisify(this.db.incrby).bind(this.db);
    this.decrby = promisify(this.db.decrby).bind(this.db);
    this.hset = promisify(this.db.hset).bind(this.db);
    this.hdel = promisify(this.db.hdel).bind(this.db);
    this.hgetall = promisify(this.db.hgetall).bind(this.db);
    this.hkeys = promisify(this.db.hkeys).bind(this.db);
    this.zadd = promisify(this.db.zadd).bind(this.db);
    this.zrem = promisify(this.db.zrem).bind(this.db);
    this.zrange = promisify(this.db.zrange).bind(this.db);
    this.zremrangebyscore = promisify(this.db.zremrangebyscore).bind(this.db);
    this.zrangebyscore = promisify(this.db.zrangebyscore).bind(this.db);
    this.zincrby = promisify(this.db.zincrby).bind(this.db);
 
    

    this.moment = require('moment');
    
    this.now = function(){
      return this.moment().format('X');
    }

    if(process.argv[2]) this.config = require(process.argv[2]);
    

    this.log = async function(prefix, code, data = []){
        var output = '';
        for(item of data){
              output += String(item).padEnd(15) + ' ';
          }
        console.log(prefix.padEnd(30) + code.padEnd(15) + output);
    }
    
    this.error = async function(eid, code, error){
         console.error(eid.padEnd(30) + code.padEnd(15));
         if(error)console.error(error);
          
     }
    
    this.info = async function(prefix, market, code, data = null){
          console.info(prefix.padEnd(30) + market.padEnd(15) + code.padEnd(15));
    }

  
      
    this.loadExchangeRates = async function(){
          var fetchedRates = {};
          var fetch = require('node-fetch');
          var response = await fetch('https://api.coinmarketcap.com/v2/ticker/?structure=array');
          var rates = await response.text();
          rates = JSON.parse(rates);
        
          Object.keys(rates.data).map(function(key, index) {
            fetchedRates[rates.data[key].symbol] = rates.data[key].quotes.USD.price;
          });
          
          this.rates = fetchedRates;
          
    }
    
    this.toUSD = function(amt,token){
            return amt * this.rates[token];
    }
    
   
    this.round = function(value, decimals) {
      var val = Number(Math.round(value+'e'+decimals)+'e-'+decimals);
        if(isNaN(val)) return 0;
        return val;
    }
    
    
    this.priceOfN = function(list, qty){
        
        for(row of list){

        }
        var k = 0;
        var total = 0;
        var marketPrice = 0;
        
        while(qty > 0 && list[k]){
            if(qty <= list[k].qty){
                  total += qty * list[k].price;
                  qty = 0;
            }else{
                  qty -= list[k].qty;
                  total += list[k].qty * list[k].price;
            }
            marketPrice = list[k].price;
            k++;
        };
         return {total: total, marketPrice: marketPrice};
         if(qty == 0) return {total: total, marketPrice: marketPrice};
         return Infinity;
    }
    
    this.qtyAtPrice = function(list, price){
        
        var k = 0;
        var qty = 0;
        while(price > 0 && list[k]){
            
           var qtyAvail = list[k].price * list[k].qty;
            
           if(qtyAvail >= price){
                 qty += price / list[k].price;
                 price = 0;
           }else{
                 price -= list[k].price * list[k].qty;
                 qty += list[k].price * list[k].qty;
           }       
            k++;
        };
         
         if(price == 0)return qty;
         return Infinity;
    }
    
    this.qtyOfList = function(list){
        var qty = 0;
        list.forEach(function(item){
            qty += item.qty;
        });
        return qty;
    }
     this.alchemize = function(i,A,B) {
                 //if(A.asks[0].qty < i || B.bids[0].qty < i ) return {ny: ny, marketAskPrice: 0, marketBidPrice: 0};
                 var priceOfAsk = Number(A.asks[0].price);
                 var priceOfBid = Number(B.bids[0].price);
                 if(priceOfAsk > 0 && priceOfAsk > 0){
                  var py = this.getPercentageChange(priceOfBid, priceOfAsk);
                  var ny = py - A.asks[0].fee - B.bids[0].fee;
                  return {ny: ny, marketAskPrice: priceOfAsk, askqty: A.asks[0].qty, marketBidPrice: priceOfBid, bidqty: B.bids[0].qty};
                 }else{
                   this.error('alchemize', 'Null Value!', null);
                  return {ny: ny, marketAskPrice: 0, marketBidPrice: 0};
                 }
               
           

    }


                         
    this.getPercentageChange = function(bid, ask){
        var pc = [(bid - ask)/ask] * 100;
        if(pc == 'Infinity') return 0;
        return pc;
    }
    
    this.getMinimumQty = function(arbitrage){
        return (arbitrage.fee / (arbitrage.avgY/100))/arbitrage.askPrice;
        }
        
    this.splitArray = function(arr, n) {
        var rest = arr.length % n, // how much to divide
        restUsed = rest, // to keep track of the division over the elements
        partLength = Math.floor(arr.length / n),
        result = [];
        for(var i = 0; i < arr.length; i += partLength) {
            var end = partLength + i,
                add = false;
    
            if(rest !== 0 && restUsed) { // should add one element for the division
                end++;
                restUsed--; // we've used one division element now
                add = true;
            }
    
            result.push(arr.slice(i, end)); // part of the array
    
            if(add) {
                i++; // also increment i in the case we added an extra element for division
            }
        }
    return result;
    }  
    

    /*https://github.com/oransel/node-talib
    var ALMA(var *Data, int Period)
{
  var m = floor(0.85*(Period-1));
  var s = Period/6.0;
  var alma = 0., wSum = 0.;
  int i;
  for (i = 0; i < Period; i++) {
    var w = exp(-(i-m)*(i-m)/(2*s*s));
    alma += Data[Period-1-i] * w;
    wSum += w;
  }
  return alma / wSum;
}*/

 
     
};
 
 Array.prototype.unique = function() {
    return this.reduce(function(accum, current) {
        if (accum.indexOf(current) < 0) {
            accum.push(current);
        }
        return accum;
    }, []);
}
