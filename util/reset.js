var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();
//RUN
(async function() {
    await reaver.connectdb(); 
    await reaver.db.collection('report_funds').remove({});
    await reaver.db.collection('log').remove({});
    await reaver.db.collection('info').remove({});
    await reaver.db.collection('error').remove({});
    await reaver.db.collection('funds').remove({});
    var funds = {
        "id": 1,
        "binance": {
            "eth": 1,
            "usdt": 300,
            "xrp": 1000,
            "btc": 0.25,
        },
        "hitbtc": {
            "eth": 1,
            "usdt": 300,
            "xrp": 1000,
            "btc": 0.25,
        },
        "huobi": {
            "eth": 1,
            "usdt": 300,
            "xrp": 1000,
            "btc": 0.25,
        },
        "okex": {
            "eth": 1,
            "usdt": 300,
            "xrp": 1000,
            "btc": 0.25,
        },
        "cobinhood": {
            "eth": 1,
            "usdt": 300,
            "xrp": 1000,
            "btc": 0.25,
        }
    }
    await reaver.db.collection('funds').insert(funds);
   
    
    reaver.client.close();
})();


