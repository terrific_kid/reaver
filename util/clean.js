var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();
//RUN
(async function() {
    await reaver.connectdb(); 
    //reaver.moment().subtract(120, 'minutes').from(reaver.moment()).format('x')
    await reaver.db.collection('log').remove( { time: { $lt: reaver.moment().subtract(120, 'minutes').format('x') } });
    reaver.client.close();
})();
