var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const WebSocket = require('ws');
var eid = 'cobinhood';
var api_key = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhcGlfdG9rZW5faWQiOiJkMWNmM2U5Yy01ZWFkLTRlZDAtOTY4Zi0wNTRhYmE0OTViMDUiLCJzY29wZSI6WyJzY29wZV9leGNoYW5nZV90cmFkZV9yZWFkIiwic2NvcGVfZXhjaGFuZ2VfdHJhZGVfd3JpdGUiLCJzY29wZV9leGNoYW5nZV9sZWRnZXJfcmVhZCJdLCJ1c2VyX2lkIjoiZDM0YmZkZWMtNmNkZi00ODBhLTgxN2YtZWEyNmE1NzM3MmFhIn0.OCY417pBFN8VGhiqgQzmCl2Er2dYbk6b0211wlTVD0c.V2:b522f4cc21ee8a31d82e04b0d07a7da77264a073e448833898b5cc08c791e511';


function exitHandler(error) {
  if(error)reaver.error('cobinhood-executor', 'exit', error);
  process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });

cobinhood = new WebSocket('wss://ws.cobinhood.com/v2/ws',{
  headers: {
        Origin: 'https://cobinhood.com',
        Authorization: api_key,
        Nonce: new Date().valueOf(),
      }
});
async function init(){
 
   cobinhood.on('message', async function(data){
        var res = JSON.parse(data);
        //Balance Update
        //Position Update
        //console.log(res.h);
    });

   cobinhood.on('open', function() {
        
        cobinhood.send(JSON.stringify({
            'action': 'subscribe',
            'type': 'balance-update',
        }));
        cobinhood.send(JSON.stringify({
            'action': 'subscribe',
            'type': 'order',
        }));
    });

}

async function buy(msg){
    console.log(msg);
return;
  cobinhood.send(JSON.stringify({
    "action": "place_order",
    "trading_pair_id": "COB-ETH",
    "type": "0",
    "price": "123.4567",
    "size": "1000.000",
    "side": "bid",
    "source": "exchange",
    "stop_price": "",        // mandatory for stop/stop-limit order
    "trailing_distance": "", // mandatory for trailing-stop order
    "id": "order_req_id1"
  }));
}

async function sell(msg){
    console.log(msg);
    return;
  cobinhood.send(JSON.stringify({
    "action": "place_order",
    "trading_pair_id": "COB-ETH",
    "type": "0",
    "price": "123.4567",
    "size": "1000.000",
    "side": "bid",
    "source": "exchange",
    "stop_price": "",        // mandatory for stop/stop-limit order
    "trailing_distance": "", // mandatory for trailing-stop order
    "id": "order_req_id1"
  }));
}


async function balance(){
  //Call JSON API
  var res = await reaver.req({
     host: 'api.cobinhood.com',
     port: 443,
     path: '/v1/wallet/balances',
     // authentication headers
     headers: {
        'authorization': api_key
     }   
  });
   
  await Promise.all(Object.values(res.result.balances).map(async function(balance) {
      return new Promise(async function(resolve, reject){
            var key = eid+balance.currency;
            var value = balance.total;
            await reaver.set(key.toLowerCase(), value);
           
            resolve(); 
        });
  }));
}


//Run
(async function(){
   
  await balance();
  await init();

  process.on('message', msg => {
    switch(msg.type){
      case 'buy':
      buy(msg);
      break;
      case 'sell':
      sell(msg);
      break;
    }
  });

})();
