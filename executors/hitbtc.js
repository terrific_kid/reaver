var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const WebSocket = require('ws');
var eid = 'hitbtc';
var hitbtc;
function exitHandler(error) {
  if(error)reaver.error('hitbtc-executor', 'exit', error);
  process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });


async function init(){
	hitbtc = new WebSocket('wss://api.hitbtc.com/api/2/ws');
	hitbtc.on('open', function() {
		 login();
		 subscribeReports();
		 getTradingBalance();
	});
	hitbtc.on('message', async function(data){
		 var res = JSON.parse(data);
		 //If Balance Update
		 if(!res.method){
		 	if(Array.isArray(res.result)){
				if(res.result[0].currency) balance(res.result);
		 	} 
		 }else{
		 	switch(res.method){
		 		case 'report':
		 		//console.log(res);
		 		getTradingBalance();
		 		break;
		 	}
		 }
	});
	hitbtc.on('exit', async function(){
		init();
	});
}	
async function login(){
	hitbtc.send(JSON.stringify({
            'method': 'login',
            'params': {
                "algo": "BASIC",
			    "pKey": "f780384c1013745e428f9f8a2ce3cbfc",
			    "sKey": "92d41f51713647a385d821a4f7182e3a"
            }
        }));
}
async function subscribeReports(){
	hitbtc.send(JSON.stringify({
            'method': 'subscribeReports',
            'params': {}
        }));
}
async function getTradingBalance(){
	hitbtc.send(JSON.stringify({
            'method': 'getTradingBalance',
            'params': {}
        }));
}

async function balance(balances){
	await Promise.all(Object.values(balances).map(async function(balance) {
    return new Promise(async function(resolve, reject){
          var key = eid+balance.currency;
          var value = balance.available;
          await reaver.set(key.toLowerCase(), value);
          resolve(); 
      });
    }));
}

async function buy(msg){
	  console.log(msg);
	  return;
	hitbtc.send(JSON.stringify({
	  "method": "newOrder",
	  "params": {
	    "clientOrderId": "57d5525562c945448e3cbd559bd068c4",
	    "symbol": "ETHBTC",
	    "side": "buy",
	    "price": "0.059837",
	    "quantity": "0.015"
	  },
	  "id": 123
	}));
}

async function sell(msg){
	  console.log(msg);
	  return;
	  
	hitbtc.send(JSON.stringify({
	  "method": "newOrder",
	  "params": {
	    "clientOrderId": "57d5525562c945448e3cbd559bd068c4",
	    "symbol": "ETHBTC",
	    "side": "sell",
	    "price": "0.059837",
	    "quantity": "0.015"
	  },
	  "id": 123
	}));
}


//Run
(async function(){
  
  await init();
  process.on('message', msg => {
    switch(msg.type){
      case 'buy':
      buy(msg);
      break;
      case 'sell':
      sell(msg);
      break;
    }
  });

})();
