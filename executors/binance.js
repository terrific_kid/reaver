var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const WebSocket = require('ws');
var crypto = require('crypto');
var eid = 'binance';
var api_key = 'dKh1FqWiDlLGVxzWWsE4a3GzSQlgaClnk9K1lXebdBrkXVc4ZiHLKukv6lBVOuWZ';
var secret = 'NXjBmcFKIHemPviszdYSU6zJ0Xlo6SpHtxcBgLpn7CO3vNs02iQSTxmliBpg7eOb';
const BinanceClient = require('node-binance-api');
var binance = new BinanceClient().options({
          APIKEY: api_key,
          APISECRET: secret,
          useServerTime: true, // If you get timestamp errors, synchronize to server time at startup
          test: true // If you want to use sandbox mode where orders are simulated
});


function exitHandler(error) {
  if(error)reaver.error('binance-executor', 'exit', error);
  process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });


async function init(){
 var key = await getListenKey();
 binance_stream = new WebSocket('wss://stream.binance.com:9443/ws/'+key);
 binance_stream.on('close', function(){ init(); }); 
 binance_stream.on('message', async function(data){
        var res = JSON.parse(data);
        //console.log(res);
    });
}

async function buy(msg){
   
}

async function sell(msg){
  
 
}


async function balance(){
  binance.balance(async function(error, balances){
    if ( error ) return console.error(error);
    await Promise.all(Object.entries(balances).map(async function(balance) {
    return new Promise(async function(resolve, reject){
          var key = eid+balance[0];
          var value = balance[1].available;
          await reaver.set(key.toLowerCase(), value);
          resolve(balance); 
      });
    }));
  });
}

async function getListenKey(){
  var res = await reaver.req({
     host: 'api.binance.com',
     port: 443,
     method: 'post',
     path: '/api/v1/userDataStream',
     // authentication headers
     headers: {
        'X-MBX-APIKEY': api_key
     }   
  });

   return res.listenKey;
}

//Run
(async function(){
   
  await balance();
  await init();

  process.on('message', msg => {
    switch(msg.type){
      case 'buy':
      buy(msg);
      break;
      case 'sell':
      sell(msg);
      break;
    }
  });

})();
