var reaver = require("./reaver-util.js");
var reaver = new reaver();


 

 (async function() {
    
      await reaver.connectdb();
      console.log('Generating Magus Report...');
            var now = reaver.moment().format('x');
            var results = await reaver.db.collection('alchemy').find().toArray();
            var count = 0;
            var avgY = 0;
            var netusd = 0;
             var exchangeA = [];
            var exchangeB = [];
            var markets = [];
            await Promise.all(results.map(async result => {
                 try{
                     var ausd = 0;
                     markets.push(result.path);
                     for(item of result.data){
                         count += 1;
                          avgY += item.py;
                          ausd += item.netusd;
                          exchangeA.push(item.exchangeA);
                          exchangeB.push(item.exchangeB);
                     }
                     ausd = ausd / result.data.length;
                     netusd += ausd;
                 
                 }catch(err){
                   console.log('***Err', err);  
                }
            }));
            
           
           avgY = avgY / count;
            
            var report = {time: now, avgY: avgY, netusd: netusd, exchangeA: exchangeA, exchangeB: exchangeB, markets: markets};
            await reaver.db.collection('report_magus').insert(report);
            console.log('Inserted Report!');
      reaver.client.close();
     
})();
