var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();


 

 (async function() {
    
      await reaver.connectdb();
    
      
      console.log('Generating Funds Report...');
            var now = reaver.moment().format('x');
            var funds = await reaver.db.collection('funds').find().toArray();
            var balance = {};
            
            for(fund of Object.values(funds)){
                for(res of Object.entries(fund)){
                    if(res[0] == '_id' || res[0] == 'id') continue;
                  
                    for(coin of Object.entries(res[1])){
                        balance[coin[0]] = (balance[coin[0]] || 0) + coin[1];
                    }
                }
            
            }
                 
            console.log(balance);       
            var report = {time: now, balance: balance};
            await reaver.db.collection('report_funds').insert(report);
            console.log('Inserted Report!');
      reaver.client.close();
     
})();
