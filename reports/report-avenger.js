var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();


 

 (async function() {
    
      await reaver.connectdb();
      console.log('Generating Avenger Report...');
            var now = reaver.moment().format('x');
            var results = await reaver.db.collection('scavenger').find().toArray();
            var count = results.length;
            var avgY = 0;
            var netusd = 0;
            var markets = [];
            var exchangeA = [];
            var exchangeB = [];
            await Promise.all(results.map(async result => {
                 try{
                    avgY += result.py;
                    netusd += result.netusd;
                    markets.push(result.symbol);
                    exchangeA.push(result.exchangeA);
                    exchangeB.push(result.exchangeB);
                 }catch(err){
                   console.log('***Err', err);  
                }
            }));
            
            avgY = avgY / count;
           
            
            var report = {time: now, avgY: avgY, netusd: netusd, markets: markets, exchangeA: exchangeA, exchangeB: exchangeB};
            await reaver.db.collection('report_avenger').insert(report);
            console.log('Inserted Report!');
      reaver.client.close();
     
})();
