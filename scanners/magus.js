var reaver = require("./reaver-util.js");
var reaver = new reaver();
const { exec } = require('child_process');
    
    //INIT 
    (async function() {
        try {
            console.log('Magus Online!');
            var currencies = ['ETH','BTC','USDT'];
            await reaver.connectdb();
            console.log('Clearing Database...');
            await reaver.db.collection('alchemy').remove({});
            var symbols = await reaver.getSymbols();
            var paths = [];
            reaver.client.close();
           
            await Promise.all(currencies.map(async (element) => { 
                 var jumps1 = [];
                console.log(element);
                await Promise.all(symbols.map(async (symbol) => { 
                    if( symbol.title.split('/')[1] == element ) jumps1.push(symbol.title); 
                 }));
                 await Promise.all(jumps1.map(async (cjump1) => { 
                   
                    var jumps2 = [];
                    await Promise.all(symbols.map(async (cs1) => { 
                        if(cs1.title.split('/')[1] == cjump1.split('/')[0]) jumps2.push(cs1.title); 
                    }));
                    await Promise.all(jumps2.map(async (cjump2) => { 
                        paths.push([cjump1, cjump2]);
                    }));
                }));
               
            }));
            paths = reaver.splitArray(paths,3);
         
            var start = reaver.moment();
            var s1 = stream(paths[0]);
            var s2 = stream(paths[1]);
            var s3 = stream(paths[2]);
           
         
            
            await Promise.all([s1,s2]);
            var stop = reaver.moment();
            console.log('Magus Complete!', 0.001 * (stop.format('x') - start.format('x')), 'seconds');
       
        }catch(err){
            console.log(err);
        }
    
    })();


            
                
                 async function stream(paths){
                       return new Promise(async function(resolve, reject) {
                           for(path of paths){
                            await alchemy(path);
                           }
                           resolve();
                       });
                 }
 


                 async function alchemy(path){
                    return new Promise(function(resolve, reject) {
                         exec('node /reaver/alchemy2.js '+path+' --max_old_space_size=2048', (err, stdout, stderr) => {
                            if (err) {
                             console.error('err',err);
                            return;
                            }
                            console.log(stdout.toString());
                            resolve();
                        });
                    });
                    
    }