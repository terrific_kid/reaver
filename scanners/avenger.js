var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();
const { exec } = require('child_process');
    
    //INIT 
    (async function() {
        try {
            console.log('Avenger Online!');
            await reaver.connectdb();
            console.log('Clearing Database...');
            await reaver.db.collection('scavenger').remove({});
            console.log('Loading Symbols...');
            var symbols = await reaver.getSymbols();
            var symbols = reaver.splitArray(symbols,3);
           
            var start = reaver.moment();
             
          
            var s1 = stream(symbols[0]);
            var s2 = stream(symbols[1]);
            var s3 = stream(symbols[2]);
         
        
       
            await Promise.all([s1,s2,s3]);
          
            
             
             
            var stop = reaver.moment();
            console.log('Avenge Complete!', 0.001 * (stop.format('x') - start.format('x')), 'seconds');
        reaver.client.close();
        }catch(err){
            console.log(err);
        }
    
    })();


            
                
                 async function stream(symbols){
                       return new Promise(async function(resolve, reject) {
                           for(symbol of symbols){
                            await scavenge(symbol);
                           }
                           resolve();
                       });
                 }
 


                 async function scavenge(symbol){
                    return new Promise(function(resolve, reject) {
                         exec('node /reaver/scanners/scavenger.js '+String(symbol.title)+' --max_old_space_size=2048', (err, stdout, stderr) => {
                            if (err) {
                             console.error('err',err);
                            return;
                            }
                            console.log(stdout.toString());
                            resolve();
                        });
                    });
                    
    }