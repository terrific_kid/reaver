var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();
    
    //INIT 
    (async function() {
     try {
        await reaver.connectdb();
        await loadSymbols();
        reaver.client.close();
      } catch(e) {
        console.error(e)
      }
    })();
    
    async function loadSymbols(){
         console.log('Loading Symbols...');
         reaver.db.collection('symbols').remove({});
              var exclude =[
         
              ]
            
             
             var currencies = [
                 'ETH',
                 'USD',
                 'BTC',
                 'USDT',
             ];
         await Promise.all(reaver.exchanges.map(async (exchange) => {
            try{
                var markets = await exchange.fetchMarkets();
                await Promise.all(markets.map(async (market) => {
                     if(currencies.indexOf(market.symbol.split('/')[1]) != -1 && exclude.indexOf(market.symbol.split('/')[0]) == -1){
                         console.log('Loading', market.symbol);
                          await reaver.db.collection('symbols').insert({id: exchange.id, symbol: market.symbol});
                          }
                }));
            }catch(err){
                //console.log(err);
            }
         }));
         console.log('Finished Loading Symbols!');
    }
    
    