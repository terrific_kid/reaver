var reaver = require("./reaver-util.js");
var reaver = new reaver();

    
    //INIT 
    (async function() {
     try {
     console.log('Alchemy Online!');
        var path = process.argv[2];
        await reaver.connectdb();
        console.log('Loading Exchange Rates...');
        await reaver.loadExchangeRates();
        var start = reaver.moment();
        console.log('Running Alchemy...');
        
        await alchemy(path);
       
       
       
        var stop = reaver.moment();
        console.log('Alchemy Complete!',path, 0.001 * (stop.format('x') - start.format('x')), 'seconds');
        reaver.client.close();
      } catch(e) {
        console.error(e)
      }
    })();
    
    
    
    async function alchemy(path){
        try{
            var path = path.split(',');
            var asks = {};
            var arbitrage = [];
            var found = false;
            await Promise.all(path.map(async (jump) => { 
               var exchanges = await reaver.getExchanges(jump);
               var jumpasks = {};
               await Promise.all(exchanges.map(async (exchange) => {
                var ask = [];                
                var askBook = await reaver.db.collection('book').findOne({id: exchange.id+jump});
                if(askBook){
                    await Promise.all(askBook.book.asks.map(async cask => {
                        ask.push({type: 'ask', symbol: jump, price: cask[0], qty: cask[1], id: exchange.id});
                    }));
                    ask = ask.sort(function(a, b) { return a.price - b.price; });
                    jumpasks[exchange.id] = ask;
                }
               }));
               asks[jump] = jumpasks;
            }));
            
           
            await Promise.all(Object.values(asks[path[0]]).map(async (askA) => {
                var askAQty = reaver.qtyOfList(askA);
               
                await Promise.all(Object.values(asks[path[1]]).map(async (askB) => {
                   if(!askB.length)return;
                   var deposit = await reaver.db.collection('health').findOne({id: askB[0].id+path[1].split('/')[1]});
                   if(!deposit) return;
                   if(!deposit.deposit) return;
                    
                    var askBQty = reaver.qtyOfList(askB);
                    var maxQty = askAQty;
                    if(maxQty >= 600000) maxQty = 600000;
                  
                    var arbs = [];
                    for(i = 0.1; i <= maxQty; i += 0.1){
                        var pricePaid = reaver.priceOfN(askA, i);
                        var qtyRecieved = reaver.qtyAtPrice(askB, i);
                        var PC = reaver.getPercentageChange(reaver.toUSD(qtyRecieved, path[1].split('/')[0]),reaver.toUSD(pricePaid, path[0].split('/')[1]));
                        if(PC > 0){
                            arbs.push({path: path, exchangeA: askA[0].id, pricePaid: pricePaid, exchangeB: askB[0].id,  py: PC, qty: i});
                        }else{
                           
                        }
                    }
                    if(arbs.length){
                        var arb = await getBestArbs(arbs);
                        arbitrage.push(arb);
                        found = true;
                    }
                }));
            }));
            
            if(found){
                    console.log('Found', arbitrage.length);
                    await reaver.db.collection('alchemy').insert({path: path, data: arbitrage});
                };
            
                            
          
        }catch(err){
            console.log(err);
        }
       
        async function getBestArbs(arbs){
            var winner = {netusd: 0};
            for(arb of arbs){
                var netusd = reaver.toUSD(arb.pricePaid * (arb.py/100),arb.path[0].split('/')[1]);
                 if(netusd > winner.netusd){
                     arb.netusd = netusd;
                     winner = arb;
                 }
            }
            return winner;
        }
    }