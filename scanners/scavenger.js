var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();

    
    //INIT 
    (async function() {
     try {
     console.log('Scavenger Online!');
        var symbol = process.argv[2];
        await reaver.connectdb();
        console.log('Loading Exchange Rates...');
        await reaver.loadExchangeRates();
        var start = reaver.moment();
        console.log('Scavenging...');
        var exchanges = await reaver.getExchanges(symbol);
        await scavenge(symbol, exchanges);
        var stop = reaver.moment();
        console.log('Scavenge Complete!',symbol, 0.001 * (stop.format('x') - start.format('x')), 'seconds');
        reaver.client.close();
      } catch(e) {
        console.error(e)
      }
    })();
    
    
    async function scavenge(symbol, exchanges){
        if(symbol.includes("$")) return;
        var token = symbol.split('/')[0];
        var base = symbol.split('/')[1];
        await Promise.all(exchanges.map(async (exchangeA) => {
            try{
                var askABook = await reaver.db.collection('book').findOne({id: exchangeA.id+symbol});
                var asksA = [];
                if(askABook){
                    await Promise.all(askABook.book.asks.map(async cask => {
                        asksA.push({type: 'ask', symbol: symbol, price: cask[0], qty: cask[1], id: exchangeA.id});
                    }));
                    asksA = asksA.sort(function(a, b) { return a.price - b.price; });
                }
                console.log('Scavenging for...', exchangeA.id, symbol);
                
                await Promise.all(exchanges.map(async (exchangeB) => {
                    if(exchangeA.id == exchangeB.id) return; 
                    var bidsBBook = await reaver.db.collection('book').findOne({id: exchangeB.id+symbol});
                    var bidsB = [];
                    if(bidsBBook){
                        await Promise.all(bidsBBook.book.bids.map(async cbid => {
                            bidsB.push({type: 'bid', symbol: symbol, price: cbid[0], qty: cbid[1], id: exchangeB.id});
                        }));
                        bidsB = bidsB.sort(function(b, a) { return a.price - b.price; });
                    }
                    console.log('Checking....', exchangeB.id+symbol);
                   
                    var bidsQty = reaver.qtyOfList(bidsB);
                    var asksQty = reaver.qtyOfList(asksA);
                    var maxQty = bidsQty;
                    if(asksQty < maxQty) maxQty = asksQty;
                    if(maxQty >= 500000) maxQty = 500000;
                    
                    var arbs = [];
                    for(i = 0.1; i <= maxQty; i += 0.1){
                        var priceOfBid = reaver.priceOfN(bidsB, i);
                        var priceOfAsk = reaver.priceOfN(asksA, i);
                        
                         var PC = reaver.getPercentageChange(priceOfBid,priceOfAsk);
                         if(PC <= 0) break;
                         arbs.push({qty: i, priceOfBid: priceOfBid, priceOfAsk: priceOfAsk, py: PC});
                    }
                   
                    if(arbs.length){
                        console.log('***Found!');
                         var arbitrage = getBestArb(arbs, base);
                         arbitrage.exchangeA = exchangeA.id;
                         arbitrage.exchangeB = exchangeB.id;
                         arbitrage.symbol = symbol;
                         if(arbitrage.py <= 50 && arbitrage.py >= 0.2)await reaver.db.collection('scavenger').insert(arbitrage);
                    }
                    
                  
                
                
                
                }));
            }catch(err){
            console.log(err);
            }
        }));
    }
    
    function getBestArb(arbs, base){
        var winner = {netusd: 0};
            for(arb of arbs){
                  var netusd = reaver.toUSD(arb.priceOfBid * (arb.py/100),base);
                  if(netusd > winner.netusd)  winner = {qty: arb.qty, priceOfBid: arb.priceOfBid, priceOfAsk: arb.priceOfAsk, netusd: netusd, py: arb.py};
            }
            
        return winner;
    }

