var reaver = require("./reaver.js");
var reaver = new reaver();


 

 (async function() {
    
     await reaver.connectdb();
     await reaver.db.collection('health').remove({});
     var symbols = await reaver.getSymbols();
       console.log('Checking System Health...');
      for(symbol of symbols){
            var exchanges = await reaver.getExchanges(symbol.title);
            await Promise.all(exchanges.map(async exchange => {
                 try{
                    var token = symbol.title.split('/')[0];
                    var deposit = await reaver.getDepositAddress(token, exchange);
                    var withdraw = await reaver.checkWithdraw(token, exchange);
                     var update = {id: exchange.id+token, deposit: deposit, withdraw: withdraw };
                    await reaver.db.collection('health').update({id: exchange.id+token},update,{upsert: true});
                    if(deposit)console.log('Inserted', update);
                }catch(err){
                   console.log('***Err', symbol.title, exchange.id, err);  
                }
            }));
      }
      
      reaver.client.close();
     
})();


