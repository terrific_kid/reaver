//Init Reaver
var reaver = require('/reaver/utility/reaver-util.js');
var reaver = new reaver();

  //INIT 
    (async function() {
     try {
        await reaver.connectdb();
        await reaver.db.collection('book').remove({});
        var symbols = await reaver.getSymbols();
            for(symbol of symbols){
            var exchanges = await reaver.getExchanges(symbol.title);
            await scanGalaxy(symbol.title,exchanges);
            }
    
        reaver.client.close();
      } catch(err) {
        console.error('Scan Galaxy Error!',err)
      }
    })();

    async function scanGalaxy(symbol,exchanges) {
        console.log('Scanning For '+symbol+'...');
        await Promise.all(exchanges.map(async item => {
            //console.log('Loading Data for', item.id+symbol);
            try{
                
                const book = await item.fetchOrderBook(symbol);
                await reaver.db.collection('book').insert({id: item.id+symbol, book: book});
                
    
                 
            }catch(err){
                    console.log('***err***', symbol, item.id, err);
            }
        }));
    }
