var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const loki = require('lokijs');
const WebSocket = require('ws');

var eid = 'hitbtc';
var hitbtc;

var token = reaver.config.token;
var base = reaver.config.base;
var symbol = token+base;
var symbol = symbol.toUpperCase();
var sequence = null;

var askHashKey = eid+'_'+token+base+'_'+'asks';
var bidHashKey = eid+'_'+token+base+'_'+'bids';


async function exitHandler(debug) {
    await reaver.del(eid+'_'+token+base);
    reaver.error(eid, 'exit', debug);
    process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });




async function init(){
    await reaver.del(askHashKey);
    await reaver.del(bidHashKey);
   

    hitbtc = new WebSocket('wss://api.hitbtc.com/api/2/ws');

    hitbtc.on('open', function() {
        reaver.log(eid, 'init');
        hitbtc.send(JSON.stringify({
            'method': 'subscribeOrderbook',
            'params': {
                'symbol': symbol
            },
            'id': 'reaver1'
        }));

        hitbtc.send(JSON.stringify({
          "method": "subscribeTrades",
          "params": {
            "symbol": symbol,
            "limit": 1
          },
          "id": 'reaver1'
        }));


        


    }); 

    hitbtc.on('message', async function(data){
        var res = JSON.parse(data);
        if(res.method == 'snapshotOrderbook'){
            //Initial Orderbook Load
            sequence = res.params.sequence;

            var asks = Object.values(res.params.ask);
            var bids = Object.values(res.params.bid);
            

            asks.forEach(async (ask) => {
                await reaver.zrem(askHashKey, ask.price);
                if(parseFloat(ask.size) > 0) await reaver.zadd(askHashKey, reaver.now(), ask.price);
            });
            
            bids.forEach(async (bid) => {
                await reaver.zrem(bidHashKey, bid.price);
                if(parseFloat(bid.size) > 0) await reaver.zadd(bidHashKey, reaver.now(), bid.price);
            });

 

            
        }else if(res.method == 'updateOrderbook'){
            if(res.params.sequence == sequence + 1){
                sequence++;
                //Incremental Orderbook Update
                var asks = Object.values(res.params.ask);
                var bids = Object.values(res.params.bid);
                
                asks.forEach(async (ask) => {
                    await reaver.zrem(askHashKey, ask.price);
                    if(parseFloat(ask.size) > 0) await reaver.zadd(askHashKey, reaver.now(), ask.price);
                });
            
                bids.forEach(async (bid) => {
                    await reaver.zrem(bidHashKey, bid.price);
                    if(parseFloat(bid.size) > 0) await reaver.zadd(bidHashKey, reaver.now(), bid.price);
                });
                
                
            }else{
             
              exitHandler('hitbtc sequence error!');
            }
        }else if(res.method == 'updateTrades'){
           
            Object.values(res.params.data).forEach(async (trade) => {
                

                //await reaver.zadd(token+base+'_trade', reaver.now(), trade.price );
            });
            

        }else if(res.method == 'snapshotTrades'){

             
        }else if(res.result){
            
        }else{
 
            exitHandler(res);
        }
    }); 
    hitbtc.on('close', function close() { init(); }); 
};

 



//RUN
(async function() {
    
    await init();
})();