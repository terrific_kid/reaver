var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const WebSocket = require('ws');
const pako = require('pako');


var eid = 'okex';

var okex;

var token = reaver.config.token;
var base = reaver.config.base;
var symbol = token+"-"+base;
var symbol = symbol.toUpperCase();

var askHashKey = eid+'_'+token+base+'_'+'asks';
var bidHashKey = eid+'_'+token+base+'_'+'bids';


async function exitHandler(debug) {
    await reaver.del(eid+'_'+token+base);
    reaver.error(eid, 'exit', debug);
    process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });

var timeCheck = 0;
 
async function init(){
    await reaver.del(askHashKey);
    await reaver.del(bidHashKey);

    okex = new WebSocket('wss://real.okex.com:10442/ws/v3');
    okex.on('open', function() {
        reaver.log('okex', 'init',[]);
        okex.send(JSON.stringify({"op": "subscribe", "args": ["spot/depth:"+token.toUpperCase()+"-"+base.toUpperCase()]}));
    });


    okex.on('message', async function(data){
        var msg = pako.inflateRaw(data, {to: 'string'});
        var res = JSON.parse(msg);
        if(!res.data) return;
        
        var asks = res.data[0].asks;
        var bids = res.data[0].bids;
        
        asks.forEach(async (ask) => {
            await reaver.zrem(askHashKey, ask[0]);
            if(parseFloat(ask[1]) > 0) await reaver.zadd(askHashKey, reaver.now(), ask[0]);
        });
            
        bids.forEach(async (bid) => {
            await reaver.zrem(bidHashKey, bid[0]);
            if(parseFloat(bid[1]) > 0) await reaver.zadd(bidHashKey, reaver.now(), bid[0]);
        });
        


        process.send({eid: eid, type: 'orderbook'});
    });
    okex.on('close', function close() {
        init();
    });
}



//RUN
(async function() {
    
    await init();
})();

