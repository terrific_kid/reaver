var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const WebSocket = require('ws');
const pako = require('pako');

var eid = 'huobi';

var huobi;
var token = reaver.config.token;
var base = reaver.config.base;
token = token.toLowerCase();
base = base.toLowerCase();
var symbol = token+base;


async function exitHandler(debug) {
    await reaver.del(eid+'_'+token+base);
    reaver.error(eid, 'exit', debug);
    process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });

async function init(){
    huobi = new WebSocket('wss://api.huobi.pro/ws');
    huobi.on('open', function() {
        reaver.log(eid, 'connected');
        huobi.send(JSON.stringify({
            "sub": `market.${symbol}.depth.step0`,
            "id": `${symbol}`
        }));
    });
    huobi.on('message', async function(data){
        let text = pako.inflate(data, { to: 'string'});
        let msg = JSON.parse(text);
        if (msg.ping) {
            huobi.send(JSON.stringify({
            pong: msg.ping
            }));
        }else if(msg.tick){
            var bids = msg.tick.bids.map(function(item){ return {price: parseFloat(item[0]), fee: 0.2, qty: parseFloat(item[1])} }).sort(function(b, a) { return a.price - b.price; });
            var asks = msg.tick.asks.map(function(item){ return {price: parseFloat(item[0]), fee: 0.2, qty: parseFloat(item[1])} }).sort(function(a, b) { return a.price - b.price; });
             await reaver.set(eid+'_'+token+base, JSON.stringify({bids: bids, asks: asks}));
            process.send({eid: eid, market: token+base, type: 'orderbook'});

        }
    });
    huobi.on('close', function close() {
        init();
    });
}



//RUN
(async function() {
    
    await init();
})();
