var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const loki = require('lokijs');
const WebSocket = require('ws');

var eid = 'cobinhood';

var cobinhood;
 
var token = reaver.config.token;
var base = reaver.config.base;
var symbol = token+'-'+base;
var symbol = symbol.toUpperCase();
var sequence = null;

var askHashKey = eid+'_'+token+base+'_'+'asks';
var bidHashKey = eid+'_'+token+base+'_'+'bids';

async function exitHandler(debug) {
    await reaver.del(eid+'_'+token+base);
    reaver.error(eid, 'exit', debug);
    process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });
 
async function init(){
 

    cobinhood = new WebSocket('wss://ws.cobinhood.com/v2/ws');
    cobinhood.on('open', function() {
        reaver.log(eid, 'connected');
        cobinhood.send(JSON.stringify({
            'action': 'subscribe',
            'type': 'order-book',
            "trading_pair_id": symbol,

        }));
    });
    cobinhood.on('message', async function(data){
        var res = JSON.parse(data);
        
        if(res.h[2] == 's' || res.h[2] == 'u'){
            var bids = res.d.bids;
            var asks = res.d.asks;
            asks.forEach(async (ask) => {
              if(parseFloat(ask[1]) > 0) await reaver.zadd(askHashKey, reaver.now(), ask[0]);
              if(parseFloat(ask[1]) < 0) await reaver.zrem(askHashKey, ask[0]);
            });
            bids.forEach(async (bid) => {
               if(parseFloat(bid[1]) > 0) await reaver.zadd(bidHashKey, reaver.now(), bid[0]);
               if(parseFloat(bid[1]) < 0) await reaver.zrem(bidHashKey, bid[0]);
            });
     
            
            sendBook();
         }else{

          console.log(res);

        }
    });
    cobinhood.on('close', function close() {
        init();
    }); 
}

async function sendBook(){
      await reaver.zremrangebyscore(askHashKey, 0, reaver.now() - 10);
        await reaver.zremrangebyscore(bidHashKey, 0, reaver.now() - 10);
     process.send({eid: eid, market: token+base, type: 'orderbook'});
}




//RUN
(async function() {
    await init();
})();