var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
const { fork } = require('child_process');
var algorithms = [];
var loaders = [];
var book;
var queue;

function exitHandler(debug) {
  
     for(child of Object.values(algorithms)) child.kill();
     for(child of Object.values(loaders)) child.kill();

     reaver.error('reaver', 'exit', debug);
    process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });


//RUN
(async function() {


    //Init Reaver
    reaver.log(reaver.config.token.toUpperCase() + ' ' + reaver.config.base.toUpperCase(), "Online!"); 
    
 



    //Launch Strategies
     await Promise.all(reaver.config.algorithm.map(async (algo) => { 
        algorithms[algo] = fork('/reaver/algorithm/'+algo+'.js', [process.argv[2]]);
        algorithms[algo].on('exit', (error) => { exitHandler(error); });
    }));
 
 
    //Start RealTime Order Book Loading
    await Promise.all(reaver.config.loaders.map(async (loader) => { 
        loaders[loader] = fork('/reaver/loaders/'+loader+'.js', [process.argv[2]]);
        loaders[loader].on('exit', (error) => { exitHandler(error); });
    }));

   


})();
 
 
 
 
 
