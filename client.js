//Init Reaver
var reaver = require('/reaver/utility/reaver-util');
var reaver = new reaver();
//Load WebServer Reqs
var express = require('express');
 
var fs = require('fs');
var https = require('https');
var privateKey  = fs.readFileSync('/reaver/ssl/domain.key', 'utf8');
var certificate = fs.readFileSync('/reaver/ssl/domain.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//RUN HTTP Server
var app = express();
var server = https.createServer(credentials, app);
var io = require("socket.io")(server);

//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//WEBSOCKETS
io.on('disconnect', function () {
     reaver.client.close();
  });
  
io.on('connection', async function (socket) {
    await reaver.connectdb();
       
       
    socket.on('logs', async function() {
        var log = await reaver.db.collection('log').find().sort({time: -1}).limit(25).toArray();
        var error = await reaver.db.collection('error').find().sort({time: -1}).limit(25).toArray();
        var info = await reaver.db.collection('info').find().sort({time: -1}).limit(25).toArray();
        socket.emit('logs', {log: log, error: error, info: info});
    });
    
    socket.on('fgraph', async function(){
        
        var report = await reaver.db.collection('report_funds').find().sort({time: 1}).toArray();
        var labels = [];
        var eths = [];
        var usdts = [];
        var xrps = [];
        var btcs = [];
        
        for(item of report){
            labels.push(parseInt(item.time));
           eths.push(item.balance.eth);
            xrps.push(item.balance.xrp);
            usdts.push(item.balance.usdt);
            btcs.push(item.balance.btc);
        }
        var now = reaver.moment().format('x');
        var funds = await reaver.db.collection('funds').find().toArray();
        
        var balance = {};
        
         for(fund of Object.values(funds)){
            for(res of Object.entries(fund)){
                
                if(res[0] == '_id' || res[0] == 'id') continue;
                for(coin of Object.entries(res[1])){
                    balance[coin[0]] = (balance[coin[0]] || 0) + coin[1];
                }
            }
        }
        
        labels.push(parseInt(now));
         
        usdts.push(balance.usdt);
        eths.push(balance.eth);
        xrps.push(balance.xrp);
        btcs.push(balance.btc);

        
        var usdReport = {
            type: 'line',
            data: {
    			labels: labels,
    			datasets: [{
                    label: 'BTC',
                    data: btcs,
                    //backgroundColor: '#FFFFFF',
                    borderColor: '#FFFFFF',
                    borderWidth: 1,
                    lineTension: 0,
                    pointRadius: 10,
                    pointStyle: 'line',
                    pointBackgroundColor: '#FFFFFF',
                    pointBorderColor: '#FFFFFF',
                 },{
    				label: 'USDT',
                    data: usdts,
                    //backgroundColor: '#FFFFFF',
                    borderColor: '#FFFFFF',
                    borderWidth: 1,
                    lineTension: 0,
                    pointRadius: 10,
                    pointStyle: 'line',
                    pointBackgroundColor: '#FFFFFF',
                    pointBorderColor: '#FFFFFF',
    			 },{
    				label: 'ETH',
                    data: eths,
                    //backgroundColor: '#FFFFFF',
                    borderColor: '#FFFFFF',
                    borderWidth: 1,
                    lineTension: 0,
                    pointRadius: 10,
                    pointStyle: 'line',
                    pointBackgroundColor: '#FFFFFF',
                    pointBorderColor: '#FFFFFF',
    			 },{
    				label: 'XRP',
                    data: xrps,
                    //backgroundColor: '#FFFFFF',
                    borderColor: '#FFFFFF',
                    borderWidth: 1,
                    lineTension: 0,
                    pointRadius: 10,
                    pointStyle: 'line',
                    pointBackgroundColor: '#FFFFFF',
                    pointBorderColor: '#FFFFFF',
    			 }],
    			 
    			 
            },
            options: {
        			 legend: {display: false}
    			 }

         };
        socket.emit('fgraph', {usdReport: usdReport});
    });
 });

server.listen(8443, async function() {
console.log('Reaver Client Listening...');
});
 