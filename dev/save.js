


async function recalculate(eid, book){
    await Promise.all(book.map(async (setA) => {
        await Promise.all(book.map(async (setB) => {
            if(setA[0] != eid && setB[0] != eid) return;
            if(setA[0] == setB[0]) return;
            var alchemy = reaver.alchemize(qty, setA[1], setB[1]);

            

            if(alchemy.ny > 0){
                execute(setA, setB, alchemy, qty, token, base);
            }else{
                //view(setA, setB, alchemy, qty, token, base);
            }
        }));
    }));
var reaver = require("/reaver/utility/reaver-util.js");
var reaver = new reaver();
const { fork } = require('child_process');
var token = process.argv[2];
var base = process.argv[3];
var forks = {};
var trades = [];

function exitHandler(error) {
    reaver.error('reaver', token+base, 'EXIT');
    for(f of Object.values(forks)) f.kill("SIGTERM");
    process.exit(0);
}
process.on("exit", exitHandler.bind() ); 
process.on("uncaughtException", exitHandler.bind() ); 
process.on("SIGINT", exitHandler.bind() );
process.on("SIGTERM", exitHandler.bind() );
process.on("SIGUSR1", exitHandler.bind() );
process.on("SIGUSR2", exitHandler.bind() );

process.on('message', message => {
      switch(message.type){
        case 'init':
        init(message.loaders, message.strategy);
        break;
      }
      //forks[message.id].send(message);
  });
  


 
  async function init(loaders, strategy){
        reaver.log('reaver', token+base, 'executor');
        console.log(strategy);
         
         //Enable Executor
         var alchemy = fork(strategy[0].path, [token, base]);
         alchemy.send({type: 'init', loaders: loaders});
  
  };   






}


async function view(setA, setB, alchemy, qty, token, base){
    
    if(alchemy.ny > -0.5)reaver.log(setA[0].padEnd(10, ' '), setB[0].padEnd(10, ' '), qty, token, parseFloat(alchemy.marketAskPrice).toFixed(8), parseFloat(alchemy.marketBidPrice).toFixed(8), base, alchemy.ny );
}


async function execute(setA, setB, alchemy, qty, token, base){
    var xpath = setA[0]+setB[0];
    if(halts[xpath]) return false;
        halts[xpath] = 2;
        var funds = await reaver.db.collection('funds').findOne({id: 1});
        funds[setA[0]][token] = parseFloat(funds[setA[0]][token]) + parseFloat(qty);
        funds[setA[0]][base] = parseFloat(funds[setA[0]][base]) - parseFloat(alchemy.marketAskPrice * qty);
        funds[setB[0]][token] = parseFloat(funds[setB[0]][token]) - parseFloat(qty);
        funds[setB[0]][base] = parseFloat(funds[setB[0]][base]) + parseFloat(alchemy.marketBidPrice * qty);
        if(funds[setA[0]][base] < 0 || funds[setB[0]][token] < 0){ halts[xpath] = 0; return false; } 
            await reaver.db.collection('funds').update({id: 1},funds);
            datalord.send({id: setA[0], xpath: xpath, type: 'buy', price: alchemy.marketAskPrice, qty: qty}); 
            datalord.send({id: setB[0], xpath: xpath, type: 'sell', price: alchemy.marketBidPrice, qty: qty});
            reaver.log(setA[0].padEnd(10, ' '), setB[0].padEnd(10, ' '), qty, token, parseFloat(alchemy.marketAskPrice).toFixed(8), parseFloat(alchemy.marketBidPrice).toFixed(8), base, alchemy.ny );
}