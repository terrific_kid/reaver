var reaver = require("./reaver.js");
var reaver = new reaver();
    
    //INIT 
    (async function() {
     try {
        var token = process.argv[2];
        await reaver.connectdb();
        await alchemy(token);
        reaver.client.close();
      } catch(e) {
        console.error(e)
      }
    })();


  
//////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//Alchemy Pathfinder 
 
    
    function round(value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    }
    async function estimate(wallet, numOfJumps){
       
         
        var keys = Object.keys(wallet);
        var OT = keys[0];
        var AT;
        var originalValue = 1;
        var alchemyValue = 0;
        Object.keys(wallet).map(function(key, indexr){
           if(wallet[key]) AT = key;
        });
        
        
        
        if(OT == AT){
            alchemyValue = wallet[AT];
        }else{
            /*
            try{
            var bestBidValue = 0;
            var bestBid = await reaver.bookdb.find({type: 'bid', symbol: AT+'/'+OT}).sort({price: -1}).limit(1).toArray();
            if(bestBid.length)bestBidValue = bestBid[0].price * wallet[AT];
             
            var bestAskvalue = 0; 
            var bestAsk = await reaver.bookdb.find({type: 'ask', symbol: OT+'/'+AT}).sort({price: 1}).limit(1).toArray();
            if(bestAsk.length) bestAskvalue = wallet[AT] / bestAsk[0].price;
            }catch(err){
                console.log(err);
            }
            alchemyValue = bestBidValue;
            if(bestAskvalue > alchemyValue) alchemyValue = bestAskvalue;
            */
         }
        
        var percentIncrease = [(alchemyValue - originalValue)/originalValue] * 100;
         var percentIncreasePerJump = percentIncrease / (numOfJumps/2);
        
        
        
        
        wallet.value = round(percentIncreasePerJump,1); 
    return wallet;
    }
    async function alchemy(token){
        console.log('Running Alchemy for '+token+'...');
       //console.log('Token0 is '+token);
        var jumps1 = await findJumps(token, reaver.symbols);
        await Promise.all(jumps1.map(async (cjump1) => {
            //console.log('Pushing Jump 1',cjump1);
            //buildWallet(cjump1);
            //console.log(cjump1[cjump1.length-1]);
            token1 = cjump1[cjump1.length-1].symbol.split('/');
            if(cjump1[cjump1.length-1].type == 'bid') token1 = token1[1];
            if(cjump1[cjump1.length-1].type == 'ask') token1 = token1[0];
            //console.log('Token1 is', token1);
            var jumps2 = await findJumps(token1, reaver.symbols);
            await Promise.all(jumps2.map(async (cjump2) => {
                
                //console.log('Pushing Jump 2',cjump1.concat(cjump2));
               await buildWallet(token,cjump1.concat(cjump2));
                token2 = cjump2[cjump2.length-1].symbol.split('/');
                 //console.log(token2);
                if(cjump2[cjump2.length-1].type == 'bid') token2 = token2[1];
                if(cjump2[cjump2.length-1].type == 'ask') token2 = token2[0];
                //console.log('Token2 is', token2);
                var jumps3 = await findJumps(token2, reaver.symbols);
               
                    await Promise.all(jumps3.map(async (cjump3) => {
                        //console.log('Pushing Jump 3',cjump1.concat(cjump2, cjump3));
                        await buildWallet(token,cjump1.concat(cjump2, cjump3));
                        token3 = cjump3[cjump3.length-1].symbol.split('/');
                        //console.log(token3);
                        if(cjump3[cjump3.length-1].type == 'bid') token3 = token3[1];
                        if(cjump3[cjump3.length-1].type == 'ask') token3 = token3[0];
                        //console.log('Token3 is', token3);
                        var jumps4 = await findJumps(token3, reaver.symbols);
                        var jumps4 = [];
                        await Promise.all(jumps4.map(async (cjump4) => {
                            //console.log('Pushing Jump 4');
                            await buildWallet(token,cjump1.concat(cjump2, cjump3, cjump4));
                          
                            token4 = cjump4[cjump4.length-1].symbol.split('/');
                            //console.log(token4);
                            if(cjump4[cjump4.length-1].type == 'bid') token4 = token4[1];
                            if(cjump4[cjump4.length-1].type == 'ask') token4 = token4[0];
                            //console.log('Token4 is', token4);
                            //var jumps5 = await findJumps(token4, reaver.symbols);
                            var jumps5 = [];
                            await Promise.all(jumps5.map(async (cjump5) => {
                                //console.log('Pushing Jump 5');
                                await buildWallet(token,cjump1.concat(cjump2, cjump3, cjump4,cjump5));
                                token5 = cjump5[cjump5.length-1].symbol.split('/');
                                if(cjump5[cjump5.length-1].type == 'bid') token5 = token5[1];
                                if(cjump5[cjump5.length-1].type == 'ask') token5 = token5[0];
                                var jumps6 = await findJumps(token5, reaver.symbols);
                                var jumps6 = [];
                                 await Promise.all(jumps6.map(async (cjump6) => {
                                    await buildWallet(token,cjump1.concat(cjump2, cjump3, cjump4,cjump5,cjump6));
                                 }));
                            }));

                        }));

                    }));
            }));
            
        }));     
         console.log('***Finished!');          
    }
    async function findJumps(token){
        
        var jumps = [];
        await Promise.all(reaver.symbols.map(async symbol => {
                         
                var jump = [];
                t = symbol.split('/');
                //Find Ask Jump
                 if(token == t[1]) jump = await reaver.bookdb.find({type: 'ask', symbol: symbol}).sort({price: 1}).limit(1).toArray(); 
                 //Find Bid Jump
                 if(token == t[0]) jump = await reaver.bookdb.find({type: 'bid', symbol: symbol}).sort({price: -1}).limit(1).toArray();
                 
                 if(jump.length == 1){ 
                     jumps.push(jump);
                     jump = null; 
                  };
              
        }));
         
        return jumps;
    }
    
    async function buildWallet(token, course){
            
           
            
            var pass = false;
            var wallet = {};
            wallet[token] = 1;
            await Promise.all(course.map(async (jump, jindex) => {
                token = jump.symbol.split('/');
                if(!wallet[token[0]])wallet[token[0]] = 0;
                if(!wallet[token[1]])wallet[token[1]] = 0;
                if(jump.type == 'bid'){
                    wallet[token[1]] = parseFloat(jump.price * wallet[token[0]]);
                    wallet[token[0]] -= wallet[token[0]];
                }
                if(jump.type == 'ask'){
                    wallet[token[0]] = parseFloat(wallet[token[1]] / jump.price);
                    wallet[token[1]] -= wallet[token[1]];
                }
            }));
            wallet = await estimate(wallet, course.length);
           
            if(wallet.value > 0.5) {
                 console.log(course);
                  var hash = crypto.createHash('md5').update(JSON.stringify(course)).digest('hex');
                  await reaver.alchemydb.update({key: hash},{path: course, wallet: wallet, time: parseInt(reaver.moment().format('x'))},{upsert: true});
            } 

    }
