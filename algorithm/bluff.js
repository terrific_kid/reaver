var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
var forks = {};
function exitHandler(debug) {
  reaver.error('bluff', 'exit', debug);
  process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });



async function monitor(){

var log = [];

  var lowestTrade = 0;
  var highestTrade = 0;
 
  var trades = await reaver.zrangebyscore(reaver.config.token+reaver.config.base+'_trade', reaver.now() - 300, '+inf');

   if(trades.length){
        lowestTrade = Math.min.apply(Math, trades);
        highestTrade = Math.max.apply(Math, trades);
      }
   var pc = reaver.getPercentageChange(highestTrade, lowestTrade);   
   log.push(trades.length, lowestTrade, highestTrade, reaver.round(pc, 3) + '%');
   

log.push(reaver.round(pc, 3) + '%');

 
var book = await reaver.mget(reaver.config.token+reaver.config.base+'_ask',reaver.config.token+reaver.config.base+'_bid');
var pc = reaver.getPercentageChange(book[1], book[0]);
log.push(reaver.round(pc, 3) + '%');
 

reaver.log('reaver','monitor',log);




}


async function calc(){
      var lowestTrade = 0;
      var highestTrade = 0;

      var trades = await reaver.zrangebyscore(reaver.config.token+reaver.config.base+'_trade', '-inf', '+inf');
      

      if(trades.length){
        lowestTrade = Math.min.apply(Math, trades);
        highestTrade = Math.max.apply(Math, trades);
      }
      
      var pc = reaver.getPercentageChange(highestTrade, lowestTrade);
      //console.log(pc);

       

       
     
      
     // await reaver.zadd('monitor', reaver.now(), JSON.stringify(['monitor',reaver.config.token+' '+reaver.config.base,[trades.length, lowestTrade, highestTrade, reaver.round(pc,3)+'%', bluff[0], bluff[1]]]));

              //ADD TRADE TO QUEUE
         // var pc = reaver.getPercentageChange(bestBid[0], bestAsk[0]);
          //if(pc > 0.1) reaver.lpush('queue', JSON.stringify([bestAsk[0], bestAsk[1], bestBid[0], bestBid[1], reaver.round((pc),3) + '%']));
         
         

       
        
}


//RUN
(async function() {
    reaver.log('bluff', 'init');
    setInterval(calc, 200);
    setInterval(monitor, 300000);
    
})();





