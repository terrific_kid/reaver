var reaver = require("/reaver/util/reaver-util.js");
var reaver = new reaver();
var forks = {};
function exitHandler(debug) {
  reaver.error('alchemy', 'exit', debug);
  process.exit(0);
}
process.on("exit", function(e){ exitHandler(e); } ); 
process.on("uncaughtException", function(e){ exitHandler(e); }); 
process.on("unhandledRejection", function(e){ exitHandler(e); }); 
process.on("SIGINT", function(e){ exitHandler(e); });
process.on("SIGTERM", function(e){ exitHandler(e); });
process.on("SIGUSR1", function(e){ exitHandler(e); });
process.on("SIGUSR2", function(e){ exitHandler(e); });

async function cache(){
    //Cache Results
        var bestAsk = false;
        var bestAskLoader = '';
        var bestBid = false;
        var bestBidLoader = '';

        await Promise.all(reaver.config.loaders.map( async function(loader){
            var askHashKey = loader+'_'+reaver.config.token+reaver.config.base+'_'+'asks';
            var bidHashKey = loader+'_'+reaver.config.token+reaver.config.base+'_'+'bids';
            var asks = await reaver.zrangebyscore(askHashKey, '-inf', '+inf');
            var bids = await reaver.zrangebyscore(bidHashKey, '-inf', '+inf');
            minAsk = Math.min.apply(Math, asks);
            maxBid = Math.max.apply(Math, bids);
           
            if(minAsk && minAsk < bestAsk || bestAsk == false ){
               bestAsk = minAsk;
               bestAskLoader = loader;
            }

            if(maxBid && maxBid > bestBid || bestBid == false){
                 bestBid= maxBid;
                 bestBidLoader = loader;
            } 
         }));        

}


async function alchemy(){
          var book = await reaver.mget(reaver.config.token+reaver.config.base+'_ask',reaver.config.token+reaver.config.base+'_ask_loader',reaver.config.token+reaver.config.base+'_bid',reaver.config.token+reaver.config.base+'_bid_loader');
          var pc = reaver.getPercentageChange(book[2], book[0]);

          if(pc > 0.2) reaver.lpush('queue', JSON.stringify([book[0], book[1], book[2], book[3], reaver.round((pc),3) + '%']));
          
           
}






//RUN
(async function() {
    reaver.log('alchemy', 'init');
 
    setInterval(alchemy, 200);
})();





